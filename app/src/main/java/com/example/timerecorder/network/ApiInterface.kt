package com.example.timerecorder.network

import com.example.timerecorder.constant.LOGIN
import com.example.timerecorder.constant.SIGN_UP
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {
    @POST(SIGN_UP)
    @JvmSuppressWildcards
    fun userSignup(@Body body: Map<String, Any>): Call<Any>

    @POST(LOGIN)
    fun userLogin(@Body body: HashMap<String, Any>): Call<Any>
}