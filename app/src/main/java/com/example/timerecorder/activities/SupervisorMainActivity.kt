package com.example.timerecorder.activities

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivitySupervisorMainBinding
import com.example.timerecorder.fragments.HomeFragment
import com.example.timerecorder.fragments.ProfileFragment
import com.example.timerecorder.fragments.TasksFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.util.*

class SupervisorMainActivity : AppCompatActivity() {
    lateinit var binding: ActivitySupervisorMainBinding
    private var mStacks: HashMap<String, Stack<Fragment>>? = null
    val HOME = "home"
    val TASKS = "tasks"
    val PROFILE = "profile"
    private var mCurrentTab: String? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivitySupervisorMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInitialize()
        setUiActions()
    }

    private fun setUiActions() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationView)
    }

    private fun getInitialize() {
        mStacks = HashMap()
        mStacks!![HOME] = Stack()
        mStacks!![TASKS] = Stack()
        mStacks!![PROFILE] = Stack()

        selectedTab(HOME)
    }

    var bottomNavigationView = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.home -> {
                selectedTab(HOME)
                true
            }
            R.id.tasks -> {
                selectedTab(TASKS)
                true
            }
            R.id.profile -> {
                selectedTab(PROFILE)
                true
            }
            else -> false
        }
    }

    override fun onBackPressed() {
        if (mStacks!![mCurrentTab!!]!!.size == 1) {
            finish()
            return
        }
        popFragments()
    }

    fun popFragments() {
        val fragment = mStacks!![mCurrentTab!!]!!.elementAt(mStacks!![mCurrentTab!!]!!.size - 2)

        mStacks!![mCurrentTab!!]!!.pop()

        val manager =
            supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.fragment_container, fragment)
        ft.commit()
    }


    private fun selectedTab(tabId: String) {
        mCurrentTab = tabId
        if (mStacks!![tabId]!!.size == 0) {
            if (tabId == HOME) {
                pushFragments(tabId, HomeFragment(), true)
            } else if (tabId == TASKS) {
                pushFragments(tabId, TasksFragment(), true)
            } else if (tabId == PROFILE) {
                pushFragments(tabId, ProfileFragment(), true)
            }
        } else {
            pushFragments(tabId, mStacks!![tabId]!!.lastElement(), false)
        }
    }

    fun pushFragments(tag: String?, fragment: Fragment?, shouldAdd: Boolean) {
        if (shouldAdd) mStacks!![tag!!]!!.push(fragment)
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.fragment_container, fragment!!)
        ft.commit()
    }

}