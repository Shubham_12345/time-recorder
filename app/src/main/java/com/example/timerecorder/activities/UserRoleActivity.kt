package com.example.timerecorder.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityUserRoleBinding

class UserRoleActivity : AppCompatActivity(), UserRoleInterface {
    lateinit var binding: ActivityUserRoleBinding

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivityUserRoleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUiActions()
    }

    private fun setUiActions() {
        binding.userRoleHandler = this
    }

    override fun onEmployeeRoleClicked() {
         startActivity(Intent(this, LoginActivity::class.java).putExtra("role", 2))
    }

    override fun onSuperviserRoleClicked() {
        startActivity(Intent(this, LoginActivity::class.java).putExtra("role", 1))
    }

}

interface UserRoleInterface{
    fun onEmployeeRoleClicked()
    fun onSuperviserRoleClicked()
}