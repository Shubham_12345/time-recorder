package com.example.timerecorder.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.app.cricketstats.config.AppPreferences
import com.example.timerecorder.R
import com.example.timerecorder.databinding.ActivityLoginBinding
import com.example.timerecorder.network.ApiClient
import com.example.timerecorder.network.ApiInterface
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception

class LoginActivity : AppCompatActivity() , LoginInterface{
    lateinit var binding: ActivityLoginBinding
    var roleType: Int? = null
    var deviceId = ""
    lateinit var appPreferences: AppPreferences


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setStatusBarColor(this.getResources().getColor(R.color.color_white))
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getInitialize()
        setUiActions()
    }

    private fun getInitialize() {
        roleType = intent.getIntExtra("role", 0)
    }

    private fun setUiActions() {
        binding.loginHandler = this
        appPreferences = AppPreferences()
        appPreferences.init(this)
    }

    override fun onLoginButtonClick() {
        var isLoginDetailsValid = validateLoginDetails()
        if (isLoginDetailsValid && roleType != 0){
            getUserLogin()
        }
    }

    private fun getUserLogin() {
        val map = HashMap<String, Any>()
        map.put("emailId", binding.emailEdt.text.toString())
        map.put("password", binding.passwordEdt.text.toString())
        map.put("role", roleType!!)
        map.put("deviceId", deviceId)
        Log.i(Companion.TAG, "getUserLogin: map "+map)
        val call = ApiClient().getClient(this)!!.create(ApiInterface::class.java)
        call.userLogin(map).enqueue(object : retrofit2.Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    if (response.body() == null) {
                        Log.i(TAG, "onResponse: ere " + response.errorBody())
                        Log.i(
                            TAG,
                            "onResponse: eerbb " + response.isSuccessful + " kkk " + response.code() + " fffd " + response.message()
                        )
                        if (response.errorBody() != null) {
                            val errorObj = JSONObject(Gson().toJson(response.errorBody()))
                            Log.i(TAG, "onResponse: error $errorObj")
                            Toast.makeText(
                                this@LoginActivity,
                                "error : $errorObj",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        val res = Gson().toJson(response.body())
                        val mainObject = JSONObject(res)
                        if (mainObject.getBoolean("success")) {
                            Toast.makeText(
                                this@LoginActivity,
                                "status : " + mainObject.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.i(TAG, "onResponse: dfdf "+mainObject)
                            val data = mainObject.getJSONObject("data")
                            Log.i(TAG, "onResponse: dfuuudf "+data)
                            appPreferences.email = data.optString("emailId")
                            appPreferences.firstName = data.optString("firstName")
                            appPreferences.lastName = data.optString("lastName")
                            appPreferences.profileUrl = data.optString("profileUrl")
                            appPreferences.token = data.optString("token")
                            appPreferences.userId = data.optInt("userId")
                            appPreferences.role = data.optInt("role")
                            appPreferences.isLogin = true
                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(
                        this@LoginActivity,
                        "exception : " + e.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(
                    this@LoginActivity,
                    "onFailure : " + t.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }

    private fun validateLoginDetails(): Boolean {
         var isLoginDetailsValid = false
        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (binding.emailEdt.text.isBlank()){

        }else if (binding.passwordEdt.text.isBlank()){

        }else if (deviceId.isBlank() || deviceId == null){
            android.widget.Toast.makeText(this,"device id is null or empty", android.widget.Toast.LENGTH_SHORT).show()
        }else{
            isLoginDetailsValid = true
        }
        return isLoginDetailsValid
    }

    override fun onSignUpScreenLinkClick() {
        startActivity(Intent(this, SignupActivity::class.java).putExtra("role", roleType))
        finish()
    }


    companion object {
        private const val TAG = "LoginActivity"
    }
}

interface LoginInterface {
    fun onLoginButtonClick()
    fun onSignUpScreenLinkClick()
}